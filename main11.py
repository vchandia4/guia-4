from secuenciador import Secuenciador
from secuenciaadn import SecuenciaADN


def main():
    secuencia = SecuenciaADN("ACGTGCATG")
    secuencia.get_complemento_inverso()
    secuencia.contar_nucleotidos()
    secuencia.calcular_peso_molecular()
    print(secuencia.get_complemento_inverso())
    print(secuencia.contar_nucleotidos())
    print(secuencia.calcular_peso_molecular())
























if __name__ == "__main__":
    main() 