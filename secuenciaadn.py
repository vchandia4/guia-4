class SecuenciaADN():
    def __init__(self, cadena):
        self.__cadena = cadena
        self.__longitud = None


    def get_cadena(self):
        return self.__cadena


    def set_cadena(self, cadena):
        if isinstance(cadena, str):
            self.__cadena = cadena

    def get_longitud(self):
        return self.__longitud


    def set_longitud(self, longitud):
        if isinstance(longitud, str):
            self.__longitud = longitud